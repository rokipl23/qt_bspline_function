#-------------------------------------------------
#
# Project created by QtCreator 2017-01-29T19:49:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = B-Spline
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bspline.cpp

HEADERS  += mainwindow.h \
    bspline.h

FORMS    += mainwindow.ui
