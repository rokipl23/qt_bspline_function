#include "bspline.h"
#include <QDebug>

Bspline::Bspline()
{

}

Bspline::Bspline(int n, double t0, double tk, int k)
   /*
    T(n+k),
    P(n+k)
        */
    {
        samples = n;
        degree = k;
        srand( time( NULL ) );
        dt=(tk-t0)/n;
        /*
        for(int j=0;j<(n+k);j++){
            T.at(j)=t0+dt*j;
            P.at(j)=(values[(std::rand() % 10)]);
            //std::cout<<T.at(j)<<std::endl;
        }
        */
    }

double Bspline::calculateValueAt(double time,std::vector<int>& P,std::vector<double>& T){
    double x = 0;
    double t = time;
    for (int i =0; i<samples; i++){
        //qDebug()<<"Time="<<t;

        x=x + P.at(i)*calculateBspline(t,i,degree,T);
       qDebug()<<"P.at("<<P.at(i)<<"  T="<<T.at(i)<<"   Bspline"<<calculateBspline(t,i,degree,T);
    }
    return x;
}

double Bspline:: getValueT(int position, std::vector<double>& T){
    if(position>(T.size()-1) || position < 0 ) return 0;
    else return T.at(position);
}

double Bspline::calculateBspline(double t, int i, int k,std::vector<double>& T){
    //qDebug()<<"TimeInBspline="<<t;

    if(k==1){
        if(t<getValueT((i+1),T) && t>getValueT(i,T)){
            //qDebug()<<"Returned 1 value";
            return 1;
        }
        else{
            //qDebug()<<"Ti="<<t<<"  ki="<<k;
           return 0;
        }
    }
    else if(k>1) {
        double L1 = (t-getValueT(i,T))*Bspline::calculateBspline(t,i,(k-1),T);
        double L2 = (getValueT((i+k),T)-t)*Bspline::calculateBspline(t,i+1,k-1,T);
        double M1 ;
        double M2 ;
        if(getValueT((i+k-1),T)==getValueT((i),T)){
            L1=0;  //zerowanie ulamka
            M1 = 1;
        }
        else M1 = getValueT((i+k-1),T)-getValueT(i,T);

        if(getValueT((i+k),T)==getValueT((i+1),T)){
            L2=0;  //zerowanie ulamka
            M2 = 1;
        }
        else M2 = getValueT((i+k),T)-getValueT((i+1),T);
        return (L1/M1)+(L2/M2);
        /*
        return (t-getValueT(i))*Bspline::calculateBspline(t,i,(k-1))/(getValueT(i+k-1)-getValueT(i))+
        (getValueT(i+k-1)*t)*Bspline::calculateBspline(t,i+1,k-1)/(getValueT(i+k)-getValueT(i+1));
        */
    }
}
