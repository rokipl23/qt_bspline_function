#ifndef BSPLINE_H
#define BSPLINE_H
#include <vector>
#include <ctime>
#include <cstdlib>
#include <iostream>

class Bspline
{
    int samples;
    int degree;
    double values[10]= {0.0, 0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1, 2.4, 2.7};
    double dt;

public:
    Bspline();
    Bspline(int n, double t0, double t1, int k);
    double calculateValueAt(double t,std::vector<int>& P,std::vector<double>& T);
    double calculateBspline(double t, int i, int k,std::vector<double>& T);
    double getValueT(int position,std::vector<double>& T);
};

#endif // BSPLINE_H
