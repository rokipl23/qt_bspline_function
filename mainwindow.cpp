#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pointTimeEdit -> setValidator( new QDoubleValidator(0,500,3, this) );
    ui->endEdit -> setValidator( new QIntValidator(0, 500, this) );
    ui->samplesEdit -> setValidator( new QIntValidator(0, 500, this) );
    ui->degreeEdit -> setValidator( new QIntValidator(0, 4, this) );

    samples = 100;
    endTime = 200;
    degree = 1;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    P.clear();
    T.clear();
    double step=endTime/(double)samples;

    for(int i = 0 ; i<samples; i++){
        int tmp = 10*sin(0.2*step*i);
        int val = (int)(tmp*10+100);
        P.push_back(val);
        T.push_back(step*i);
        qDebug()<<T.at(i);

    }
}

void MainWindow::on_calculate_clicked()
{
    P.clear();
    T.clear();
    double step=endTime/(double)samples;
    for(int i = 0 ; i<samples; i++){
        int tmp = 10*sin(0.2*step*i);
        int val = (int)(tmp*10+100);
        P.push_back(val);
        T.push_back(step*i);
        //qDebug()<<T.at(i);

    }
    Bspline * bspline = new Bspline(samples,0,endTime,degree );
    qDebug()<<"Got it! calculating...";
    qDebug()<<bspline->calculateValueAt(pointTime,P,T);

}

void MainWindow::on_samplesEdit_editingFinished()
{
    QString tmp=ui->samplesEdit->text();
    samples=tmp.toInt();
}

void MainWindow::on_degreeEdit_editingFinished()
{
    QString tmp=ui->degreeEdit->text();
    degree=tmp.toInt();
}

void MainWindow::on_endEdit_editingFinished()
{
    QString tmp=ui->endEdit->text();
    endTime=tmp.toInt();
}

void MainWindow::on_pointTimeEdit_editingFinished()
{
    QString tmp=ui->pointTimeEdit->text();
    pointTime=tmp.toDouble();
    qDebug()<<pointTime;
}

void MainWindow::on_endEdit_returnPressed()
{
    //not used
}
