#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QMainWindow>
#include <QFileDialog>
#include <QDebug>

#include <math.h>       /* sin */
#include "bspline.h"
#define PI 3.14159265

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int endTime;
    int degree;
    int samples;
    double pointTime;
    std::vector<int> P;
    std::vector<double> T;




private slots:
    void on_pushButton_clicked();

    void on_calculate_clicked();

    void on_samplesEdit_editingFinished();

    void on_degreeEdit_editingFinished();

    void on_endEdit_returnPressed();

    void on_endEdit_editingFinished();

    void on_pointTimeEdit_editingFinished();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
