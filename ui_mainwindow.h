/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *calculate;
    QLineEdit *pointTimeEdit;
    QLabel *label;
    QPushButton *pushButton;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLineEdit *samplesEdit;
    QLineEdit *degreeEdit;
    QLineEdit *endEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        calculate = new QPushButton(centralWidget);
        calculate->setObjectName(QStringLiteral("calculate"));
        calculate->setGeometry(QRect(30, 30, 81, 22));
        pointTimeEdit = new QLineEdit(centralWidget);
        pointTimeEdit->setObjectName(QStringLiteral("pointTimeEdit"));
        pointTimeEdit->setGeometry(QRect(30, 10, 81, 21));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(130, 10, 251, 191));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(0, 190, 81, 22));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(0, 70, 110, 108));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        samplesEdit = new QLineEdit(widget);
        samplesEdit->setObjectName(QStringLiteral("samplesEdit"));

        verticalLayout->addWidget(samplesEdit);

        degreeEdit = new QLineEdit(widget);
        degreeEdit->setObjectName(QStringLiteral("degreeEdit"));

        verticalLayout->addWidget(degreeEdit);

        endEdit = new QLineEdit(widget);
        endEdit->setObjectName(QStringLiteral("endEdit"));

        verticalLayout->addWidget(endEdit);

        MainWindow->setCentralWidget(centralWidget);
        calculate->raise();
        pointTimeEdit->raise();
        label->raise();
        samplesEdit->raise();
        degreeEdit->raise();
        endEdit->raise();
        samplesEdit->raise();
        pushButton->raise();
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 19));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        calculate->setText(QApplication::translate("MainWindow", "calculate", 0));
        pointTimeEdit->setText(QApplication::translate("MainWindow", "position", 0));
        label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Draw", 0));
        samplesEdit->setText(QApplication::translate("MainWindow", "num of samples", 0));
        degreeEdit->setText(QApplication::translate("MainWindow", "degree", 0));
        endEdit->setText(QApplication::translate("MainWindow", "end time", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
